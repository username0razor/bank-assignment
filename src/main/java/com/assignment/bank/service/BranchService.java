package com.assignment.bank.service;

import com.assignment.bank.model.BranchDto;
import com.assignment.bank.response.CommonResponseObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by shivam on 11/09/18
 */
public interface BranchService {
  CommonResponseObject<BranchDto> getBranchDetails(String ifsc);
  CommonResponseObject<Page<BranchDto>> getBranchDetails(String bankName, String city, Pageable pageable);
}
