package com.assignment.bank.repository;

import com.assignment.bank.entity.Branch;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by shivam on 11/09/18
 */
@Repository("com.assignment.bank.repository.BranchRepository")
public interface BranchRepository extends JpaRepository<Branch, String> {
  Branch findByIfscCode(String ifsc);
  Page<Branch> findByBankNameAndCity(String bankName, String city, Pageable pageable);
}
