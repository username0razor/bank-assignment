package com.assignment.bank.constants;

/**
 * Created by shivam on 11/09/18
 */
public class BranchConstants {
  public static final String BANK_BRANCH_TABLE = "bank_branches";
  public static final String IFSC = "ifsc";
  public static final String BANK_ID = "bank_id";
  public static final String BRANCH_NAME = "branch";
  public static final String ADDRESS = "address";
  public static final String CITY = "city";
  public static final String DISTRICT = "district";
  public static final String STATE = "state";
  public static final String BANK_NAME = "bank_name";
}
