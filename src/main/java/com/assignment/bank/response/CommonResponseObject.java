package com.assignment.bank.response;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.http.HttpStatus;

/**
 * Created by shivam on 11/09/18
 */
@Getter
@Setter
@ToString(doNotUseGetters = true)
public class CommonResponseObject<T> {

  private T response;
  private HttpStatus statusCode;

  public CommonResponseObject(HttpStatus status) {
    this.statusCode = status;
  }
}