package com.assignment.bank.model;

import com.assignment.bank.entity.Branch;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

/**
 * Created by shivam on 11/09/18
 */
@Getter
@Setter
@NoArgsConstructor
public class BranchDto {

  private String ifscCode;
  private Long bankId;
  private String branch;
  private String address;
  private String city;
  private String district;
  private String state;
  private String bankName;

  public BranchDto(Branch branch) {
    this.ifscCode = branch.getIfscCode();
    this.bankId = branch.getBankId();
    this.branch = branch.getBranch();
    this.address = branch.getAddress();
    this.city = branch.getCity();
    this.district = branch.getDistrict();
    this.state = branch.getState();
    this.bankName = branch.getBankName();
  }

  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }
}
