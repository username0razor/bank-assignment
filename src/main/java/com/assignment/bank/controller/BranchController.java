package com.assignment.bank.controller;

import com.assignment.bank.model.BranchDto;
import com.assignment.bank.response.CommonResponseObject;
import com.assignment.bank.service.BranchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * Created by shivam on 11/09/18
 */

@RestController
@RequestMapping(value = "/v1")
public class BranchController {

  @Autowired
  @Qualifier("com.assignment.bank.service.impl.BranchServiceImpl")
  private BranchService branchService;

  @RequestMapping(value = "/branch/ifsc/{ifscCode}", method = RequestMethod.GET)
  public ResponseEntity<CommonResponseObject> getBranchDetails(
      @PathVariable("ifscCode") String ifscCode) {
    CommonResponseObject<BranchDto> student = branchService.getBranchDetails(ifscCode);
    return new ResponseEntity<>(student, student.getStatusCode());
  }

  @RequestMapping(value = "/branch", method = RequestMethod.GET)
  public ResponseEntity<CommonResponseObject> getBranchDetail(
      @RequestParam("city") String city,
      @RequestParam("bankName") String bankName,
      @RequestParam(value = "pageSize", defaultValue = "20") Integer pageSize,
      @RequestParam(value = "pageNumber", defaultValue = "1") Integer pageNumber) {
    /**
     * pageNumber - 1 ; because page index starts from 0;
     * TODO: code will break for less than 0
     */
    Pageable pageable = new PageRequest(pageNumber - 1 , pageSize < 1 ? 1 : pageSize);
    CommonResponseObject<Page<BranchDto>> student =
        branchService.getBranchDetails(bankName, city, pageable);
    return new ResponseEntity<>(student, student.getStatusCode());
  }
}
