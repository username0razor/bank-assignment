package com.assignment.bank.entity;

import com.assignment.bank.constants.BranchConstants;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by shivam on 11/09/18
 */
@Entity
@Table(name = BranchConstants.BANK_BRANCH_TABLE)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Branch {

  @Id
  @Column(name = BranchConstants.IFSC)
  private String ifscCode;

  @Column(name = BranchConstants.BANK_ID)
  private Long bankId;

  @Column(name = BranchConstants.BRANCH_NAME)
  private String branch;

  @Column(name = BranchConstants.ADDRESS)
  private String address;

  @Column(name = BranchConstants.CITY)
  private String city;

  @Column(name = BranchConstants.DISTRICT)
  private String district;

  @Column(name = BranchConstants.STATE)
  private String state;

  @Column(name = BranchConstants.BANK_NAME)
  private String bankName;

  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }
}
