package com.assignment.bank.service.impl;

import com.assignment.bank.entity.Branch;
import com.assignment.bank.model.BranchDto;
import com.assignment.bank.repository.BranchRepository;
import com.assignment.bank.response.CommonResponseObject;
import com.assignment.bank.service.BranchService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by shivam on 11/09/18
 */
@Service("com.assignment.bank.service.impl.BranchServiceImpl")
@Slf4j
public class BranchServiceImpl implements BranchService {

  @Autowired
  @Qualifier("com.assignment.bank.repository.BranchRepository")
  private BranchRepository branchRepository;

  @Override
  public CommonResponseObject<BranchDto> getBranchDetails(String ifsc) {
    CommonResponseObject responseObject = new CommonResponseObject(HttpStatus.OK);
    Branch branch = branchRepository.findByIfscCode(ifsc);
    if (branch != null) {
      responseObject.setResponse(new BranchDto(branch));
    }
    return responseObject;
  }

  @Override
  public CommonResponseObject<Page<BranchDto>> getBranchDetails(String bankName, String city,
      Pageable pageable) {
    CommonResponseObject<Page<BranchDto>> responseObject =
        new CommonResponseObject<>(HttpStatus.OK);
    Page<BranchDto> branchDtos = new PageImpl<>(Collections.emptyList());
    responseObject.setResponse(branchDtos);
    if (StringUtils.isNotEmpty(bankName) && StringUtils.isNotEmpty(city)) {
      Page<Branch> byBankNameAndCity =
          branchRepository.findByBankNameAndCity(bankName, city, pageable);
      if (!CollectionUtils.isEmpty(byBankNameAndCity.getContent())) {
        List<BranchDto> branchDtoList = new ArrayList<>();
        byBankNameAndCity.getContent().stream().forEach(branch -> {
          branchDtoList.add(new BranchDto(branch));
        });
        responseObject.setResponse(new PageImpl<>(branchDtoList));
      }
    }
    return responseObject;
  }


}
